# U-Boot: Raspberry Pi 4
# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Kevin Mihelich <kevin@archlinuxarm.org>

pkgname=uboot-raspberrypi
pkgver=2021.10
pkgrel=1
pkgdesc="U-Boot for Raspberry Pi 4"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
makedepends=('bc' 'dtc' 'git')
conflicts=('linux-rpi4' 'linux-rpi4-mainline' 'uboot')
provides=('uboot')
install=${pkgname}.install
_commit=a490197f0672d948860b2b807884ae65eabc4d4f
#commit-rpi4b=b0f9870274333af93ecc5e779619bf51c092364d

source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2"
        "https://github.com/raspberrypi/firmware/raw/${_commit}/boot/bcm2710-rpi-3-b.dtb"
        "https://github.com/raspberrypi/firmware/raw/${_commit}/boot/bcm2710-rpi-3-b-plus.dtb"
        "https://github.com/raspberrypi/firmware/raw/${_commit}/boot/bcm2710-rpi-cm3.dtb"
        "https://github.com/raspberrypi/firmware/raw/${_commit}/boot/bcm2711-rpi-4-b.dtb"
        "https://github.com/raspberrypi/firmware/raw/${_commit}/boot/bcm2711-rpi-cm4.dtb"
        "https://github.com/raspberrypi/firmware/raw/${_commit}/boot/overlays/miniuart-bt.dtbo"
        "https://github.com/raspberrypi/firmware/raw/${_commit}/boot/overlays/dwc2.dtbo"
        '0001-rpi-increase-space-for-kernel.patch'
        'config.txt')

md5sums=('f1392080facf59dd2c34096a5fd95d4c'
         '140a6706528e87482aee9dfdd490641b'
         '89e5ef34bc9895ac60705d0fa4720971'
         '0b43431f9c7a854cbcffd4a29d8d3e96'
         'b4ba6b95178a5d411fca7b0bf402234b'
         'ee9d76b20855e7469eebedc0c12cc0cb'
         'caef061ac48013d265d8376827f408ec'
         '4aa9859b5d08c5f59112cf54b1f83932'
         'aeb43f1e01983ceb8675380338ec56e8'
         '387a6a9909f954754ff0f7f39e1b1f89')

prepare() {
  cd u-boot-${pkgver/rc/-rc}

  patch -p1 -i ../0001-rpi-increase-space-for-kernel.patch
}

build() {
  cd u-boot-${pkgver/rc/-rc}

  unset CFLAGS
  unset CXXFLAGS
  unset CPPFLAGS

  make distclean
  make rpi_arm64_config
  echo 'CONFIG_IDENT_STRING=" MANJARO-ARM"' >> .config
  echo 'CONFIG_CMD_BOOTMENU=y' >> .config
  echo 'CONFIG_CMD_BOOTEFI=y' >> .config
  make EXTRAVERSION=-${pkgrel}
}

package() {
  cd u-boot-${pkgver/rc/-rc}

  mkdir -p "${pkgdir}"/boot/extlinux

  cp u-boot.bin ${pkgdir}/boot/kernel8.img
  cp ../*.dtb ${pkgdir}/boot
  mkdir -p ${pkgdir}/boot/overlays
  cp ../*.dtbo ${pkgdir}/boot/overlays

  cp ../config.txt "${pkgdir}"/boot
}
